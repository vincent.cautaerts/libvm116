This crate provides a library to send data on DMX channels when using
a Velleman vm116 (also known as K8062 DMX Controller).

It should also be compatible with Whadda WML116 (which seems to be the same product, just renamed).

Library inspired by the one found in Velleman SDK (which was in Pascal).

This library has been tested on Linux (`Ubuntu 22.04`),
Raspberry Pi (`Raspbian GNU/Linux 11 (bullseye)`) and MacOS (`High Sierra, 10.13.6`)

# Goal
Control various equipments (typically lighting, smoke emitters,...) connected with the DMX protocol
(and DMX cables) to a USB controller VM116.

The device is reported (eg: by executing `lsusb`) as having those IDs:
* Vendor ID: 0x10cf
* Product ID: 0x8062
```text
 ID 10cf:8062 Velleman Components, Inc. K8062 DMX Controller Rev 3.0
```

# Inspiration & sources
* <https://www.velleman.eu/downloads/files/downloads/k8062_dll_source.zip>
and its translation in C
* <https://www.velleman.eu/support/downloads/?code=WML116>
* <http://k8062forlinux.sourceforge.net/>

# Basic usage

See in the 'examples' folder

```rust
# fn main() -> Result<(), libvm116::Vm116Error> {
use libvm116::prelude::*;
let vm116 = Vm116::new()?;

let mut dmx_state = DmxState::new(512)?;
dmx_state.set(1, 255)?;
dmx_state.set(2, 0)?;
dmx_state.set(3, 255)?;

vm116.send(&dmx_state)?;
# Ok(())
# }
```

# Cross-compilation for Raspberry Pi

## Initial setup

The script `install_raspberry.sh` can be used to cross-compile for the Raspberry Pi in
a docker image.

 * install `docker`
 * install `cross` (to help cross-compile)
```bash
./install_raspberry.sh setup:install-cross
```
 * build the initial docker image
```bash
./install_raspberry.sh setup:build-docker-image
```

## Updates
 * Edit `install_raspberry.sh` to write the address of the Pi on the network
 * Rebuild
```bash
./install_raspberry.sh build
```
 * Send to the Pi
```bash
./install_raspberry.sh scp
```
 * Run on the Pi, eg:
```bash
ssh pi@192.168.1.22
./scenario scenario.txt
```

Since `env_logger` is used, you might run
```bash
export RUST_LOG=debug
```
before running the `scenario` program to get some debuggin logs.

# Authorization & udev (Linux & Raspberry Pi)

With Linux (i86 and Raspberry Pi), in case of permission problems
(reported as 'FailedToOpenDevice'), make a new file
`/etc/udev/rules.d/vm116.rules` with the following content:

```text
SUBSYSTEM=="usb_device", SYSFS{idVendor}=="10cf", MODE="0666"
SUBSYSTEM=="usb", ATTRS{idVendor}=="10cf", MODE="0666"
```

You will need to unplug/replug the device for this to take effect.

Note that this will allow any local user to use the device. If your machine is shared,
you might want to restrict those permissions ! See
<https://www.kernel.org/pub/linux/utils/kernel/hotplug/udev/udev.html> for details.

