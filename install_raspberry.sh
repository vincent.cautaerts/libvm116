#!/usr/bin/env sh

# Note: see file '.dockerignore' to avoid sending the whole 'target' folders to docker

# configure with your Pi IP address (or DNS)
PI_HOSTNAME=pi@cipherRa.local
# PI_HOSTNAME=pi@192.168.1.22

while true; do

WHAT=$1
shift

case "$WHAT" in
'')
	exit 0
	;;

0|setup:build-docker-image)
	docker build -t cipherbrain/libvm116:001 .
	;;

setup:install-cross)
	cargo install cross
	;;

bash|debug:bash_in_docker)
	docker run -it cipherbrain/libvm116:001 bash
	;;

build)
	echo "*** Building in docker..."
	RUSTFLAGS='-C link-arg=-s' PKG_CONFIG_ALLOW_CROSS=1 cross build --release --target armv7-unknown-linux-gnueabihf --all-targets || exit 1
	;;

scp)
	echo "*** Sending to ${PI_HOSTNAME}..."
	scp target/armv7-unknown-linux-gnueabihf/release/examples/{scenario,basic} examples/scenario.txt ${PI_HOSTNAME}:
	;;

esac

done
