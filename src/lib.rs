#![doc = include_str!("../Readme.md")]

use std::array::TryFromSliceError;
use std::error::Error;
use std::fmt::Display;

use hidapi_rusb::{HidApi, HidDevice};

pub mod prelude {
    //! re-export what is most useful to library users.
    pub use super::MAX_CHANNELS;
    pub use super::{DmxState, Vm116, Vm116Error};
}

/// Maximum number of channels
pub const MAX_CHANNELS: usize = 512;

const DMX_HEADER_START: u8 = 4;
const DMX_HEADER_SINGLE_DATA: u8 = 3;
const DMX_HEADER_DATA: u8 = 2;
const DMX_HEADER_UNUSED_THEN_DATA: u8 = 5;

/// The different errors which could occur when using this library.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Vm116Error {
    /// More than 512 channels were requested
    TooManyChannels,
    /// An invalid channel was referenced, ie more than 512 or more than was was created
    InvalidChannel,
    /// Can't open the HidApi library
    CantMakeHidApi,
    /// Can't open the device. See doc for access permission problems.
    FailedToOpenDevice,
    /// Error while encoding the channels. This shouldn't happen.
    AlgorithmError,
    /// Error when sending the data to thh VM116.
    FailedToSend,
    /// Error when parsing packets. Only used in tests for now.
    ProtocolError,
}

impl Display for Vm116Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}

impl Error for Vm116Error {}

#[derive(Copy, Clone, Debug)]
enum DmxPacket<'l> {
    Start { zero: u8, values: &'l [u8; 6] },
    Data { values: &'l [u8; 7] },
    SingleData { value: u8 },
    UnusedThenData { zero: u8, values: &'l [u8; 6] },
}
impl<'l> DmxPacket<'l> {
    pub fn encode(&self, buf: &mut [u8; 8]) {
        match self {
            // Must start the messge.
            // Encodes 'zero' empty channels, followed by 6 channels
            DmxPacket::Start { zero, values } => {
                buf[0] = DMX_HEADER_START;
                buf[1] = zero + 1;
                buf[2] = values[0];
                buf[3] = values[1];
                buf[4] = values[2];
                buf[5] = values[3];
                buf[6] = values[4];
                buf[7] = values[5];
            }
            // encodes 7 channels
            DmxPacket::Data { values } => {
                buf[0] = DMX_HEADER_DATA;
                buf[1] = values[0];
                buf[2] = values[1];
                buf[3] = values[2];
                buf[4] = values[3];
                buf[5] = values[4];
                buf[6] = values[5];
                buf[7] = values[6];
            }
            // encodes just one channel
            DmxPacket::SingleData { value } => {
                buf[0] = DMX_HEADER_SINGLE_DATA;
                buf[1] = *value;
                /* Is a single data forcing other channels to 0 ? */
                // buf[2] = 0u8;
                // buf[3] = 0u8;
                // buf[4] = 0u8;
                // buf[5] = 0u8;
                // buf[6] = 0u8;
                // buf[7] = 0u8;
            }
            // encodes 'n' empty channels, followed by 6 channels
            DmxPacket::UnusedThenData { zero, values } => {
                buf[0] = DMX_HEADER_UNUSED_THEN_DATA;
                buf[1] = *zero;
                buf[2] = values[0];
                buf[3] = values[1];
                buf[4] = values[2];
                buf[5] = values[3];
                buf[6] = values[4];
                buf[7] = values[5];
            }
        }
    }
}

/// Represent the state of what has to be sent on the channels
#[derive(Clone, PartialEq, Debug)]
pub struct DmxState {
    /// `self.channels[0]` is the value to be sent for channel 1 (ie: the first channel)
    channels: Vec<u8>, // 0 < len < 512
}

impl DmxState {
    pub fn new(mut max: u16) -> Result<DmxState, Vm116Error> {
        if max > 512 {
            Err(Vm116Error::TooManyChannels)
        } else {
            if max < 6 {
                max = 6
            }
            Ok(DmxState {
                channels: vec![0u8; max as usize],
            })
        }
    }
    /// Set the given value for the channel.
    /// The channel number must be between 1 and 'max' (included),
    /// i.e minimum 1 and maximum 512.
    pub fn set(&mut self, channel: u16, value: u8) -> Result<(), Vm116Error> {
        if channel == 0 {
            Err(Vm116Error::InvalidChannel)
        } else {
            match self.channels.get_mut((channel - 1) as usize) {
                Some(v) => {
                    *v = value;
                    Ok(())
                }
                None => Err(Vm116Error::InvalidChannel),
            }
        }
    }

    pub fn reset(&mut self) {
        self.channels.iter_mut().for_each(|x| *x = 0u8);
    }

    // calculate the number of empty channels starting (and including) 'start',
    //  but leaves at least 6 channels (even empty) before the end.
    // Never returns more than 254.
    fn calc_zero(&self, start: usize) -> u8 {
        let mut cur = start;
        let last_ok = std::cmp::min(self.channels.len() - 6, start + 254);
        while cur < last_ok && self.channels[cur] == 0 {
            cur += 1;
        }
        (cur - start) as u8
    }

    fn make_packets(&self) -> Result<Vec<DmxPacket>, Vm116Error> {
        // the original algorithm from Velleman library was more efficient,
        //  but I find my implementation clearer...

        let mut packets = vec![];
        let size = self.channels.len();

        // start packet : indicates number of 0 channels, then 6 channels
        let mut n = 0;
        let z = self.calc_zero(n);
        n += z as usize;
        let first = DmxPacket::Start {
            zero: z,
            values: self.channels[n..=n + 5].try_into()?,
        };
        packets.push(first);
        n += 6;

        while n < size - 7 {
            let z = self.calc_zero(n);
            // log::debug!(" z={z} n={n} size={size}");
            if z > 0 {
                n += z as usize;
                packets.push(DmxPacket::UnusedThenData {
                    zero: z,
                    values: self.channels[n..=n + 5].try_into()?,
                });
                n += 6;
            } else {
                packets.push(DmxPacket::Data {
                    values: self.channels[n..=n + 6].try_into()?,
                });
                n += 7;
            }
        }

        // last remaining channels, sent one by one
        while n < size {
            packets.push(DmxPacket::SingleData {
                value: self.channels[n],
            });
            n += 1;
        }

        Ok(packets)
    }

    #[allow(dead_code)]
    fn parse_packets(&mut self, packets: &[DmxPacket]) -> Result<(), Vm116Error> {
        let mut n = 0usize;
        if packets.is_empty() {
            return Ok(());
        }
        if !matches!(packets[0], DmxPacket::Start { .. }) {
            return Err(Vm116Error::ProtocolError);
        }
        for packet in packets {
            match packet {
                DmxPacket::Start { zero, values } => {
                    n = 0usize;
                    for i in 0..*zero {
                        self.channels[i as usize] = 0u8;
                    }
                    n += *zero as usize;
                    for v in *values {
                        self.channels[n] = *v;
                        n += 1;
                    }
                }
                DmxPacket::Data { values } => {
                    for v in *values {
                        self.channels[n] = *v;
                        n += 1;
                    }
                }
                DmxPacket::SingleData { value: data } => {
                    self.channels[n] = *data;
                    n += 1;
                }
                DmxPacket::UnusedThenData { zero, values } => {
                    for i in n..n + (*zero as usize) - 1 {
                        self.channels[i as usize] = 0u8;
                    }
                    n += *zero as usize;
                    for v in *values {
                        self.channels[n] = *v;
                        n += 1;
                    }
                }
            }
        }
        Ok(())
    }
}

/// Represents the VM116 USB DMX controller
pub struct Vm116 {
    dev: HidDevice,
}
impl Vm116 {
    pub fn new() -> Result<Vm116, Vm116Error> {
        let vid = 0x10cf;
        let pid = 0x8062;

        let api = HidApi::new().map_err(|_err| Vm116Error::CantMakeHidApi)?;

        let dev = api
            .open(vid, pid)
            .map_err(|_err| Vm116Error::FailedToOpenDevice)?;

        Ok(Vm116 { dev })
    }

    fn send_raw(&self, data: &[u8]) -> Result<usize, Vm116Error> {
        // log::debug!("Writing data: {:02x?}", data);
        self.dev
            .write(data)
            .map_err(|_err| Vm116Error::FailedToSend)
    }

    pub fn send(&self, dmx_state: &DmxState) -> Result<u8, Vm116Error> {
        let packets = dmx_state.make_packets()?;

        let mut buf = [0u8; 8];
        let mut n_packets = 0;
        let mut first = None;
        for packet in packets {
            packet.encode(&mut buf);
            self.send_raw(&buf)?;
            n_packets += 1;
            if first.is_none() {
                first.get_or_insert(packet);
            }
        }

        // we finish by re-sending the first packet. If I don't do that, nothing is shown until
        //   a new message is sent...
        if let Some(f) = first {
            f.encode(&mut buf);
            self.send_raw(&buf)?;
            n_packets += 1;
        }

        Ok(n_packets)
    }
}

impl From<TryFromSliceError> for Vm116Error {
    fn from(_: TryFromSliceError) -> Self {
        Vm116Error::AlgorithmError
    }
}

#[cfg(test)]
mod tests {
    use super::prelude::*;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_make_packets() -> Result<(), Vm116Error> {
        init();

        let mut dmx_state = DmxState::new(512)?;

        for n in 1..10 {
            match n {
                1 => {
                    dmx_state.set(2, 0)?;
                    dmx_state.set(3, 255)?;
                }
                2 => {
                    dmx_state.set(1, 255)?;
                    dmx_state.set(3, 0)?;
                }
                3 => {
                    dmx_state.set(260, 126)?;
                }
                4 => {
                    dmx_state.set(312, 42)?;
                }
                5 => {
                    dmx_state.set(260, 0)?;
                }
                6 => {
                    dmx_state.set(512, 4)?;
                }
                7 => {
                    dmx_state.set(506, 4)?;
                }
                8 => {
                    dmx_state.reset();
                    dmx_state.set(512, 4)?;
                }
                9 => {
                    dmx_state.reset();
                    dmx_state.set(511, 4)?;
                }
                _ => {}
            }

            let packets = dmx_state.make_packets()?;
            log::debug!("Packets: {:?}", packets);
            let mut new_state = DmxState::new(512)?;
            new_state.parse_packets(&packets)?;

            assert_eq!(dmx_state, new_state);
        }

        Ok(())
    }
}
