//! Minimal program, just for demo purposes.
//!
//! When using the lib, dont re-open a new device for each command, it would
//!  slow down things too much! Reuse the 'vm116' !

use libvm116::prelude::*;

pub fn main() -> Result<(), Vm116Error> {
    env_logger::init();

    let first_channel = 1;

    let vm116 = Vm116::new()?;

    let mut dmx_state = DmxState::new(512)?;
    dmx_state.set(0 + first_channel, 255)?;
    dmx_state.set(1 + first_channel, 0)?;
    dmx_state.set(2 + first_channel, 255)?;

    vm116.send(&dmx_state)?;

    Ok(())
}
